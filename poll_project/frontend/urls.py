from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),

    path('create/', views.create, name='create'),
    path('vote/<poll_id>/', views.vote, name='vote'),
    path('results/<poll_id>/', views.results, name='results'),

    path('viewchart/<poll_id>/', views.viewChart, name='viewchart'),
    path('result_data/<poll_id>/', views.resultData, name='result_data'),

    path('email_newsletter/', views.emailNewsletter, name='email_newsletter'),
    path('activate_email_newsletter/<uidb64>/<token>', views.activateEmailNewsletter, name='activate_email_newsletter'),

    path('loginpage/', views.loginpage, name='loginpage'),
    path('logoutpage/', views.logoutpage, name='logoutpage'),
]
