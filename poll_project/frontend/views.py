from django.shortcuts import render, redirect
from django.http import HttpResponse

from poll.forms import CreatePollForm, EmailNewsLetterForm
from poll.models import Poll, UserProfile
from django.contrib.auth.models import User

from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from django.contrib import messages

from django.http import JsonResponse



def home(request):
    polls = Poll.objects.all()
    context = {
        'polls' : polls
    }
    return render(request, 'poll/home.html', context)


@login_required(login_url='loginpage')
def create(request):
    if request.method == 'POST':
        form = CreatePollForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = CreatePollForm()
    context = {
        'form' : form
    }
    return render(request, 'poll/create.html', context)

def vote(request, poll_id):
    poll = Poll.objects.get(pk=poll_id)

    if request.method == 'POST':

        selected_option = request.POST['poll']
        if selected_option == 'option1':
            poll.option_one_count += 1
        elif selected_option == 'option2':
            poll.option_two_count += 1
        elif selected_option == 'option3':
            poll.option_three_count += 1
        else:
            return HttpResponse(400, 'Invalid form')

        poll.save()

        return redirect('vote', poll.id)

    context = {
        'poll' : poll
    }
    return render(request, 'poll/vote.html', context)

def results(request, poll_id):
    poll = Poll.objects.get(pk=poll_id)
    context = {
        'poll' : poll
    }
    return render(request, 'poll/results0.html', context)

############# Emailer
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from django.template.loader import render_to_string
from poll.tokens import email_activation_token
from poll.models import Newsletter
from django.core.mail import send_mail
from django.conf import settings


def emailNewsletter(request):
    if request.method == 'POST':
        form = EmailNewsLetterForm(request.POST)
        if form.is_valid():
            email = form.save(commit=False)
            email.email_confirmed = False
            email.save()

            current_site = get_current_site(request)
            subject = "Welcome to the Polls App"
            message = render_to_string(
                'poll/emailer/newsletter_active_email.html',
                {
                    'email': email,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(email.pk)),
                    'token': email_activation_token.make_token(email),
                })
            email_address = request.POST.get('email')
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [email_address, 'georgemodestosmtpa@gamil.com']

            send_mail(subject,message,email_from,recipient_list)
            return render(request, 'poll/emailer/please_confirm_newsletter.html')
        else:
            form = EmailNewsLetterForm()
            return redirect('home')
    context = {}
    return render(request, 'poll/emailer/newsletter.html')


def activateEmailNewsletter(request, uidb64, token):
    try:
        print("Stage1")
        uid = force_text(urlsafe_base64_decode(uidb64))

        email = Newsletter.objects.get(id=uid)

    except(TypeError, ValueError, OverflowError, Newsletter.DoesNotExist):
        email = None
        print("Stage2")
    if email is not None and email_activation_token.check_token(email,token):
        email.email_confirmed = True
        email.save()
        print("Stage3")
        return render(request, 'poll/emailer/thankyounewsletter.html')
    else:
        return render(request, 'poll/emailer/invalid_news_letter.html')

##############


def loginpage(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            userprofile = UserProfile.objects.get(user=user)
            request.session['display_picture'] = userprofile.display_picture.url
            return redirect('create')
        else:
            messages.info(request, "Username OR Password is incorrect")
    context = {}
    return render(request, 'poll/auth_sec/login.html')

def logoutpage(request):
    try:
        del request.session['display_picture']
    except KeyError:
        pass

    logout(request)
    return redirect('loginpage')


def viewChart(request, poll_id):
    poll = Poll.objects.get(id=poll_id)
    context = {
        'poll':poll
    }
    return render (request, 'poll/chart_view.html', context)


def resultData(request,poll_id):
    votedata = Poll.objects.get(id=poll_id)

    data = [
        {votedata.option_one:votedata.option_one_count},
        {votedata.option_two:votedata.option_two_count},
        {votedata.option_three:votedata.option_three_count},
    ]
    return JsonResponse(data, safe=False)
