from django.contrib.auth.tokens import PasswordResetTokenGenerator
import six

class NewsletterActivationGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, email, timestamp):
        return (
            six.text_type(email.pk) + six.text_type(timestamp) +
            six.text_type(email.email_confirmed)
        )
email_activation_token = NewsletterActivationGenerator()
