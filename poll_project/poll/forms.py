from django.forms import ModelForm

from .models import Poll, Newsletter

class CreatePollForm(ModelForm):
    class Meta:
        model = Poll
        fields = ['question', 'option_one', 'option_two', 'option_three']


class EmailNewsLetterForm(ModelForm):
    class Meta:
        model = Newsletter
        fields = ('email',)
