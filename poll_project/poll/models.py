from django.db import models
from django.contrib.auth.models import User


class Poll(models.Model):
    question = models.TextField()
    option_one = models.CharField(max_length=30)
    option_two = models.CharField(max_length=30)
    option_three = models.CharField(max_length=30)
    option_one_count = models.IntegerField(default=0)
    option_two_count = models.IntegerField(default=0)
    option_three_count = models.IntegerField(default=0)

    logo = models.ImageField(upload_to = 'logo/', null=True, blank=True)

    def image_tag(self):
        from django.utils.html import escape
        return u'<img src="%s" />' % escape(self.display_picture.url)

    image_tag.allow_tags = True
    image_tag.short_Description = 'Image'

    def total(self):
        return self.option_one_count + self.option_two_count + self.option_three_count


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    contact_number = models.CharField(max_length=100)

    display_picture = models.ImageField(upload_to = 'profile_images/', null=True, blank=True)

    def image_tag(self):
        from django.utils.html import escape
        return u'<img src="%s" />' % escape(self.display_picture.url)

    image_tag.allow_tags = True
    image_tag.short_Description = 'Image'

    def __str__(self):
        return self.user.username



class Newsletter(models.Model):
    email = models.EmailField(max_length=200, null=True)
    email_confirmed = models.BooleanField(default = False)
