from django.contrib import admin
from .models import Poll, UserProfile, Newsletter
# Register your models here.
admin.site.register(Poll)

admin.site.register(UserProfile)

admin.site.register(Newsletter)
