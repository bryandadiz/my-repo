from django.urls import path

from . import views

urlpatterns = [
        path('', views.apiOverview, name="api-overview"),
        path('poll-list/', views.pollList, name="poll-list" ),
        path('poll-create/', views.pollCreate, name='poll-create'),
        path('vote-detail/<str:poll_id>/', views.voteDetail, name='vote-detail'),
        path('vote-api/<str:poll_id>', views.voteApi, name='vote-api'),
]
