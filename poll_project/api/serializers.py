from rest_framework import serializers
from poll.models import Poll

class PollsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = '__all__'
        #fields = ['option_one','option_two','option_three',]

class PollsChartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = ['option_one','option_two','option_three',]
