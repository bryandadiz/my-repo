from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .serializers import PollsSerializer

from poll.models import Poll

@api_view(['GET'])
def apiOverview(request):
	api_urls = {
		'poll-list':'/poll-list/',
        'poll-create':'/poll-create/',
        'vote-detail':'/vote-detail/<str:poll_id>/',
        'vote-api':'/vote-api/<str:poll_id>',
	}
	return Response(api_urls)

@api_view(['GET'])
def pollList(request):
    poll = Poll.objects.all()
    serializer = PollsSerializer(poll, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def pollCreate(request):
    serializer = PollsSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['GET'])
def voteDetail(request,poll_id):
    poll = Poll.objects.get(id=poll_id)
    serializer = PollsSerializer(poll, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def voteApi(request,poll_id):
    poll = Poll.objects.get(id=poll_id)
    serializer = PollsSerializer(instance=poll, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)
