from django.forms import ModelForm
from django import forms
from .models import Question, Choice

class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ['questions_text',]


class ChoiceForm(ModelForm):
    class Meta:
        model = Choice
        #fields = '__all__'
        fields = ['question','choice_text','votes',]
