from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('question_form', views.questionForm, name='question_form'),
    path('detail/<int:question_id>', views.detail, name='detail'),
    path('detail_v2/<int:question_id>', views.detail_v2, name='detail_v2'),

    path('update_question/<int:question_id>/', views.updateQuestion, name='update_question'),
    path('delete_question/<int:question_id>/', views.deleteQuestion, name='delete_question'),

    path('choice_def/<int:pk>/', views.choiceDef, name='choice_def'),
    path('choice_form', views.choiceForm, name='choice_form'),
    path('update_choice/<int:choice_id>', views.updateChoice, name='update_choice'),
    path('delete_choice/<int:choice_id>', views.deleteChoice, name='delete_choice')

]
