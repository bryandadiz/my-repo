from django.db import models

# Create your models here.
class Question(models.Model):
    questions_text = models.CharField(max_length = 200)
    pub_date = models.DateTimeField('date published')


class Person(models.Model):
    first_name = models.CharField(max_length = 200)
    last_name = models.CharField(max_length = 200)
